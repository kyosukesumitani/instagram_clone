Rails.application.routes.draw do

  root 'static_pages#home'
  
  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks',
              sessions: 'users/sessions', registrations: 'users/registrations', passwords: 'users/passwords' }

  resources :users, only: [:show, :edit, :update]
  resources :photo_posts, only: [:new, :show, :create, :destroy]
  resources :relationships, only: [:create, :destroy]
  resources :comments, only: [:create, :destroy]
  resources :likes, only: [:create, :destroy]
  
end
