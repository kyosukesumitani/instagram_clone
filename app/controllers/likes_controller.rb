class LikesController < ApplicationController
  before_action :authenticate_user!, only: [:create, :destroy]

  def create
    photo_post = PhotoPost.find_by(id: params[:photo_post_id])
    Like.create(user_id: current_user.id, photo_post_id: params[:photo_post_id])
    redirect_to photo_post
  end
  
  def destroy
    photo_post = Like.find(params[:id]).photo_post
    photo_post.unlike(current_user)
    redirect_to photo_post
  end
  

end
