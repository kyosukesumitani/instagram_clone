class UsersController < ApplicationController
  before_action :authenticate_user!, only: [:edit, :update]
  before_action :correct_user, only: [:edit, :update]
    
  def show
    @user = User.find(params[:id])
  end
  
  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(update_user_params)
      flash[:success] = "プロフィールが保存されました。"
      redirect_to edit_user_path(@user)
    else
      render 'edit'
    end
  end
  
  private
  
    def update_user_params
      params.require(:user).permit(:email, :fullname, :username, :avatar,
                                    :website, :introduction, :sex)
    end
    

  
  
end
