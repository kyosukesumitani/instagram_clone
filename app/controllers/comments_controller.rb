class CommentsController < ApplicationController
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_user, only: [:destroy]
  
  def create
    comment = current_user.comments.build(comment_params)
    @user = current_user
    @feed_items = current_user.feed.paginate(page: params[:page])
    if comment.save
      flash[:success] = 'コメントしました'
      redirect_to comment.photo_post
    else
      render 'static_pages/home'
    end
  end
  
  def destroy
    comment = Comment.find(params[:id])
    comment.destroy
    flash[:success] = "コメントを削除しました。"
    redirect_to photo_post_path comment.photo_post
  end
  
  private
  
    def comment_params
      params.require(:comment).permit(:photo_post_id, :content)
    end
  
    def correct_user
      comment = current_user.comments.find_by(id: params[:id])
      redirect_to root_url if comment.nil?
    end
end
