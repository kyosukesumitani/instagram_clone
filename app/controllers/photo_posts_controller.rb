class PhotoPostsController < ApplicationController
  before_action :authenticate_user!, only: [:new, :create, :destroy]
  before_action :correct_user, only: [:destroy]
  
  def show
    @photo_post = PhotoPost.find(params[:id])
  end
  
  def new
    @photo_post = current_user.photo_posts.build
  end
  
  def create
    @photo_post = current_user.photo_posts.build(photo_post_params)
    if @photo_post.save
      flash[:success] = "投稿しました。"
      redirect_to @photo_post
    else
      render 'new'
    end
  end
  
  def destroy
    PhotoPost.find(params[:id]).destroy
    flash[:success] = "投稿を削除しました。"
    redirect_to root_url
  end
  
  private
    
    def photo_post_params
      params.require(:photo_post).permit(:photo, :description)
    end
    
    def correct_user
      photo = current_user.photo_posts.find_by(id: params[:id])
      redirect_to root_url if photo.nil?
    end
end
