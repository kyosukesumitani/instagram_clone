class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :omniauthable, omniauth_providers: %i[facebook]
  validates :username, presence: true, length: { maximum: 30 }, uniqueness: true
  has_many :photo_posts, dependent: :destroy
  has_many :active_relationships, class_name: 'Relationship',
                              foreign_key: 'follower_id', dependent: :destroy
  has_many :following,
          through: 'active_relationships',
           source: 'followed'
  has_many :passive_relationship, class_name: 'Relationship',
                            foreign_key: 'followed_id', dependent: :destroy
  has_many :followers,
          through: 'passive_relationship',
           source: 'follower'
  mount_uploader :avatar, AvatarUploader
  validate :picture_size
  has_many :comments, dependent: :destroy
  has_many :likes, dependent: :destroy
  has_many :liked_posts, through: 'likes', source: 'photo_post'

  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end
  
  def follow(other_user)
    following << other_user
  end
  
  def unfollow(other_user)
    active_relationships.find_by(followed_id: other_user.id).destroy
  end
  
  def following?(other_user)
    following.include?(other_user)
  end
  
  def feed
    following_ids = "SELECT followed_id FROM Relationships WHERE follower_id= :user_id"
    PhotoPost.where("user_id IN (#{following_ids}) OR user_id = :user_id", user_id: id)
  end
  
  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create! do |user|
      user.email = auth.info.email
      user.password = Devise.friendly_token[0, 20]
      user.username = auth.info.name   # assuming the user model has a name
      user.avatar = auth.info.image # assuming the user model has an image
      # If you are using confirmable and the provider(s) you use validate emails, 
      # uncomment the line below to skip the confirmation emails.
      # user.skip_confirmation!
    end
  end
  
  private
  
    def picture_size
      if avatar.size > 4.megabytes
        errors.add(:avatar, "4メガバイトよりも小さい容量にしてください。")
      end
    end
end
