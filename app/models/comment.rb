class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :photo_post
  default_scope -> { order(created_at: :desc) }
  validates :user_id, presence: true
  validates :photo_post_id, presence: true
  validates :content, presence: true
  
end
