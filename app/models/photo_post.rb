class PhotoPost < ApplicationRecord
  belongs_to :user
  validates :user_id, presence: true
  validates :description, length: { maximum: 140 }
  default_scope -> { order(created_at: :desc) }
  mount_uploader :photo, PhotoUploader
  validate :picture_size
  has_many :comments, dependent: :destroy
  has_many :likes, dependent: :destroy
  
  def unlike(user)
    likes.find_by(user_id: user).destroy
  end
  
  def liked?(user)
    !likes.find_by(user_id: user.id).nil?
  end

  private
  
    def picture_size
      if photo.size > 4.megabytes
        errors.add(:photo, "4メガバイトよりも小さい容量にしてください。")
      end
    end
  
  
end
