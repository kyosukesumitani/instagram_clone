class Like < ApplicationRecord
  belongs_to :user
  belongs_to :photo_post
  validates :user_id, presence: true
  validates :photo_post, presence: true
  default_scope -> { order(created_at: :desc) }
  
end
