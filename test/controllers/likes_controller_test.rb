require 'test_helper'

class LikesControllerTest < ActionDispatch::IntegrationTest
  include Warden::Test::Helpers
  
  test "create should require logged-in user" do
    assert_no_difference 'Like.count' do
      post likes_path
    end
    assert_not flash.empty?
    assert_redirected_to new_user_session_path
  end  
  
  test "destroy should require logged-in user" do
    assert_no_difference 'Like.count' do
      delete like_path(likes(:one))
    end
    assert_not flash.empty?
    assert_redirected_to new_user_session_path
  end
end
