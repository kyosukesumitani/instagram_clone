require 'test_helper'

class PhotoPostsControllerTest < ActionDispatch::IntegrationTest
  include Warden::Test::Helpers

  def setup
    @user = users(:michael)
    @other_user = users(:archer)
  end
    
  test "should redirect new when not logged in" do
    get new_photo_post_path
    assert_not flash.empty?
    assert_redirected_to new_user_session_path
  end
  
  test "should redirect create when not logged in" do
    assert_no_difference 'PhotoPost.count' do
      post photo_posts_url, params: { photo_post: { photo: "12345",
                                              description: "sample photo" } }
    end
    assert_redirected_to new_user_session_path
  end
  
  test "should redirect destroy when not logged in" do
    assert_no_difference 'PhotoPost.count' do
      delete photo_post_url(@user.photo_posts.first)
    end
    assert_not flash.empty?
    assert_redirected_to new_user_session_path
  end
  
  test "should redirect destroy when wrong user" do
    login_as(@user, scope: :user)
    assert_no_difference 'PhotoPost.count' do
      delete photo_post_url(@other_user.photo_posts.first)
    end
    assert_redirected_to root_url
  end
  
end
