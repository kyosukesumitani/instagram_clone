require 'test_helper'

class CommentsControllerTest < ActionDispatch::IntegrationTest
  include Warden::Test::Helpers
  
  def setup
    @user = users(:michael)
    @photo_post = photo_posts(:most_recent)
  end
  
  test "should redirect create when not logged in" do
    assert_no_difference 'Comment.count' do
      post comments_path, params: { comment: { user_id: @user.id,
                                          photo_post_id: @photo_post.id,
                                          content: "good" } }
    end
    assert_redirected_to new_user_session_path
  end
  
  test "should destroy create when not logged in" do
    assert_no_difference 'Comment.count' do
      delete comment_path(comments(:most_recent))
    end
    assert_redirected_to new_user_session_path
  end  
  
  test "should destroy create when wrong user" do
    login_as(@user, scope: :user)
    assert_no_difference 'Comment.count' do
      delete comment_path(comments(:three))
    end
    assert_redirected_to root_url
  end
  
end
