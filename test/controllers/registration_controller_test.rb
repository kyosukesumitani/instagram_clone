require 'test_helper'

class RegistrationsControllerTest < ActionDispatch::IntegrationTest
  include Warden::Test::Helpers
  
  def setup
    @user = users(:michael)
    @other_user = users(:archer)
  end

  test "should get signup page" do
    get new_user_registration_url
    assert_response :success
  end
  
  test "should get edit_password" do
    login_as(@user, scope: :user)
    get edit_user_registration_path
    assert_response :success
  end
  
  test "should redirect edit_password when not logged in" do
    get edit_user_registration_path
    assert_not flash.empty?
    assert_redirected_to new_user_session_path
  end
  
   
  test "should redirect destroy when not logged in" do
    assert_no_difference 'User.count' do 
      delete user_registration_path
    end
    assert_not flash.empty?
    assert_redirected_to new_user_session_path
  end
  
  test "should redirect update when not logged in" do
    patch user_registration_path, params: { user: { current_password: 'password',
                                                                password: 'password1',
                                                   password_confirmation: 'password1' } }
    assert_not flash.empty?
    assert_redirected_to new_user_session_path
  end
end
