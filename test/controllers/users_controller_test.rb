require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  include Warden::Test::Helpers
  
  def setup
    @user = users(:michael)
    @other_user = users(:archer)
  end
  
  test "should redirect edit when not logged in" do
    get edit_user_path @user
    assert_not flash.empty?
    assert_redirected_to new_user_session_path
  end
  
  test "should redirect update when not logged in" do
    patch user_path @user, params: { user: { fullname: 'foobar',
                                              username: 'foobar',
                                               website: 'https://foo.com', 
                                          introduction: 'I am a hero.',
                                                 email: 'foobar@baz.com',
                                                   sex: '男性' } }
    assert_not flash.empty?
    assert_redirected_to new_user_session_path
  end
  
  test "should redirect edit when logged in as wrong user" do
    login_as(@user, scope: :user)
    get edit_user_path @other_user
    assert flash.empty?
    assert_redirected_to root_url
  end
  
  test "should redirect update when logged in as wrong user" do
    login_as(@user, scope: :user)
    patch user_path @other_user, params: { user: { fullname: 'foobar',
                                              username: 'foobar',
                                               website: 'https://foo.com', 
                                          introduction: 'I am a hero.',
                                                 email: 'foobar@baz.com',
                                                   sex: '男性' } }
    assert flash.empty?
    assert_redirected_to root_url
  end
  
end
