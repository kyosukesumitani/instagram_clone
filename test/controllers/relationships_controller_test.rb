require 'test_helper'

class RelationshipsControllerTest < ActionDispatch::IntegrationTest
  include Warden::Test::Helpers
  
  test "create should require logged-in user" do
    assert_no_difference 'Relationship.count' do
      post relationships_url
    end
    assert_not flash.empty?
    assert_redirected_to new_user_session_path
  end  
  
  test "destroy should require logged-in user" do
    assert_no_difference 'Relationship.count' do
      delete relationship_url(relationships(:one))
    end
    assert_not flash.empty?
    assert_redirected_to new_user_session_path
  end
end
