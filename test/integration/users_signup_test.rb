require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  
  test "invalid signup information" do
    get new_user_registration_path
    assert_no_difference 'User.count' do
      post user_registration_path(params: { user: { username: "",
                                                           email: "user@invalid",
                                                        password: "foo",
                                           password_confirmation: "bar" } } )
    end
    assert_template 'users/registrations/new'
    assert_select "div#error_explanation"
    assert_select "div.field_with_errors"
  end
  
  test "valid signup information" do
    get new_user_registration_path
    username = "UserExample"
    email = "user@invalid.com"
    assert_difference 'User.count', 1 do
      post user_registration_path(params: { user: { username: username,
                                                       email: email,
                                                    password: "password",
                                       password_confirmation: "password" } } )
    end
    follow_redirect!
    assert_template root_path
    assert_select "p.alert"
    assert_select "p.alert-info"
    assert_not User.find_by(username: username, email: email).nil?
  end
end
