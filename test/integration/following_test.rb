require 'test_helper'

class FollowingTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  
  def setup
    @user = users(:michael)
    @other_user = users(:archer)
    sign_in @user
  end
  
  test "user show interface" do
    get user_url(@other_user)
    assert_match @other_user.followers.count.to_s, response.body
    assert_match @other_user.following.count.to_s, response.body
    assert_select "form[action='/relationships']"
  end
  
  test "should follow a user" do
    assert_difference '@user.following.count', 1 do
      post relationships_url, params: { followed_id: @other_user.id }
    end
  end
  
  test "should unfollow a user" do
    @user.follow(@other_user)
    relationship = @user.active_relationships.find_by(followed_id: @other_user.id)
    assert_difference '@user.following.count', -1 do
      delete relationship_url(relationship)
    end
  end
  
  test "following and followers interface" do
    get user_path @user
    @other_user.followers.each do |f|
      assert_select 'a[href=?]', user_path(f.id), text: f.username
    end
    @user.following.each do |f|
      assert_select 'a[href=?]', user_path(f.id), text: f.username
    end
  end
end
