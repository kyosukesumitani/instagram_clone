require 'test_helper'

class AddLikeTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  
  def setup
    @user = users(:michael)
    @photo_post = photo_posts(:cat_video)
  end
  
  test "like interface" do
    sign_in @user
    get root_url
    assert_difference 'Like.count', 1 do
      post likes_path, params: { photo_post_id: @photo_post.id }
    end
    like = @photo_post.likes.find_by(user_id: @user.id)
    assert_difference 'Like.count', -1 do
      delete like_path(like)
    end
  end
end
