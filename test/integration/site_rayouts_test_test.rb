require 'test_helper'

class SiteRayoutsTestTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  
  def setup
    @user = users(:michael)
  end
  
  test "should layout links" do
    get root_path
    assert_select "a[href=?]", root_path, count: 2
    assert_select "a[href=?]", new_user_session_path, count: 2
  end
  
  test "should layout links with logged in" do
    sign_in(@user)
    get root_path
    assert_template 'static_pages/home'
    assert_select "a[href=?]", root_path, count: 2
    assert_select "a[href=?]", user_path(@user), count: 2
    assert_select "a[href=?]", new_photo_post_url
  end
end
