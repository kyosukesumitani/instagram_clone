require 'test_helper'

class PhotoPostInterfaceTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  def setup
    @user = users(:michael)
    @photo_post = photo_posts(:most_recent)
  end
  
  test "photo post interface" do
    sign_in(@user)
    get new_photo_post_url
    assert_select 'input[type="file"]'
    photo = fixture_file_upload('/files/BBQ.jpg', 'image/jpeg')
    assert_difference 'PhotoPost.count', 1 do
      post photo_posts_url, params: { photo_post: { photo: photo,
                                              description: 'BBQ photo' } }
    end
    assert assigns(:photo_post).photo?
    user_is_photo_post = assigns(:photo_post)
    assert_redirected_to user_is_photo_post
    assert_not flash.empty?
    @user.photo_posts.reload
    follow_redirect!
    assert_template 'show'
    assert_select 'a[href=?]', photo_post_url(user_is_photo_post)
    get user_url @user
    assert_match @user.photo_posts.count.to_s, response.body
    @user.photo_posts.each do |p|
      assert_select 'img[src=?]', p.photo.thumb.url
    end
    get root_path
    @user.feed.each do |p|
      assert_select 'img[src=?]', p.photo.url
      assert_select 'a[href=?]', photo_post_url(p)
      assert_select 'a[href=?]', user_path(p.user)
      assert_select 'a[href=?]', user_path(p.user), text: p.user.username
      assert_select 'span.timestamp'
    end
  end
  
  test "comment test" do
    sign_in @user
    get root_path
    assert_no_difference 'Comment.count' do
      post comments_path,  params: { comment: { user_id: @user.id,
                                          photo_post_id: @photo_post.id,
                                                content: "" } }
    end
    assert_template 'static_pages/home'
    content = "Goood!!!!"
    assert_difference 'Comment.count', 1 do
      post comments_path,  params: { comment: { user_id: @user.id,
                                          photo_post_id: @photo_post.id,
                                                content: content } }
    end
    assert_redirected_to photo_post_url(@photo_post)
    follow_redirect!
    assert_select 'span', text: content
    comment = @user.comments.find_by(photo_post_id: @photo_post.id)
    get photo_post_path(@photo_post)
    assert_select 'a[href=?]', comment_path(comment)
    assert_select 'span', text: comment.content
    assert_difference 'Comment.count', -1 do
      delete comment_path(comment)
    end
    assert_redirected_to photo_post_path comment.photo_post
  end
end
