require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  
  def setup
    @user = users(:michael)
  end

  test "login with invalid information" do
    post new_user_session_path, params: { user: { eamil: "",
                                                  password: "" } }
    assert_template 'users/sessions/new'
    assert_not flash.empty?
    get root_path
    assert flash.empty?
  end
  
  test "login with valid information followed by logout" do
    get new_user_session_path
    post user_session_path, params: { user: { email: 'michael@example.com',
                                       password: 'password' } }
    assert_equal assigns[:user], @user
    assert_redirected_to root_url
    follow_redirect!
    assert_template 'static_pages/home'
    delete destroy_user_session_path
    assert_not assigns[:user]
    assert_redirected_to root_url
    delete destroy_user_session_path
    assert_redirected_to root_url
  end
  
end
