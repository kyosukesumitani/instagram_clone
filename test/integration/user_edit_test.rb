require 'test_helper'

class UserEditTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  
  def setup
    @user = users(:michael)
  end
  
  test "unsuccessful edit" do
    sign_in(@user)
    get edit_user_path @user
    assert_template 'users/edit'
    patch user_path(@user), params: { user: { fullname: "",
                                              username: "",
                                               website: "aa", 
                                          introduction: "Iam a hero.",
                                                 email: "foooobar",
                                                   sex: "" } }
    assert_select 'div#error_explanation'
    assert_select 'div.alert'
  end
  
  test "successful edit" do
    get edit_user_path @user
    assert_redirected_to new_user_session_path
    sign_in(@user)
    fullname = "foooobar"
    username = "foobarrr"
    website = "https://foo.com"
    introduction = "I am a hero."
    email = "foooobar@baz.com"
    sex = "男性"
    photo = fixture_file_upload('/files/BBQ.jpg', 'image/jpeg')
    patch user_path(@user), params: { user: { fullname: fullname,
                                              username: username,
                                                avatar: photo,
                                               website: website, 
                                          introduction: introduction,
                                                 email: email,
                                                   sex: sex } }
    assert_not flash.empty?
    assert_redirected_to edit_user_path(@user)
    @user.reload
    assert_equal fullname, @user.fullname
    assert_equal username, @user.username
    assert_equal website, @user.website
    assert_equal introduction, @user.introduction
    assert_equal email, @user.email
    assert_equal sex, @user.sex
  end
  

  test "unsuccessful edit password" do
    sign_in(@user)
    get edit_user_registration_path
    assert_template 'users/registrations/edit'
    patch user_registration_path, params: { user: { 
                                       current_password: "password123",
                                               password: "password",
                                  password_confirmation: "password" } }
    assert_select 'div#error_explanation'
    patch user_registration_path, params: { user: { 
                                       current_password: "password",
                                               password: "a",
                                  password_confirmation: "abde" } }
    assert_select 'div#error_explanation'
  end
  
  test "successful edit password" do
    get edit_user_registration_path
    assert_redirected_to new_user_session_path
    sign_in(@user)
    new_password = "password1"
    patch user_registration_path, params: { user: { 
                                password_authentication: "password",
                                               password: new_password,
                                  password_confirmation: new_password } }
    assert_not flash.empty?
  end
  
  test "successful destroy user" do
    sign_in @user
    get edit_user_path @user
    assert_select 'a', text: 'アカウントを削除しますか'
    assert_difference 'User.count', -1 do
      delete user_registration_path
    end
    assert_not flash.empty?
  end
  
end
