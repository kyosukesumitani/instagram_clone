require 'test_helper'

class LikeTest < ActiveSupport::TestCase
  
  def setup
    @user = users(:michael)
    @photo_post = photo_posts(:cat_video)
    @like = @user.likes.build(photo_post_id: @photo_post.id)
  end
  
  test "should be valid" do
    assert @like.valid?
  end
  
  test "user_id should be present" do
    @like.user_id = nil
    assert_not @like.valid?
  end
  
  test "photo_post_id should be present" do
    @like.photo_post_id = nil
    assert_not @like.valid?
  end
  
  test "order should be most recent first" do
    assert_equal Like.first, likes(:most_recent)
  end
end
