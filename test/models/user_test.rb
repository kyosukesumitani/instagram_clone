require 'test_helper'

class UserTest < ActiveSupport::TestCase
  
  def setup
    @user = User.new(fullname: "Example User", username: "User",
              email: "user@example.com", password: "password",
              password_confirmation: "password", website: "https://foobar.com",
              introduction: "I am Example User. I am a business person",
              sex: "male")
  end
  
  test "should valid user" do
    assert @user.valid?
  end
  
  test "fullname should not be present" do
    @user.fullname = " "
    assert @user.valid?
  end
  
  test "username should not be too long" do
    @user.username = "a"*31
    assert_not @user.valid?
  end
  
  test "username should be present" do
    @user.username = " "
    assert_not @user.valid?
  end
  
  test "username should be unique" do
    other = @user.dup
    @user.save
    other.username = @user.username
    assert_not other.valid?
  end
  
  test "email should be present" do
    @user.email = " "
    assert_not @user.valid?
  end
  
  test "email should be unique" do
    other = @user.dup
    @user.save
    other.email = @user.email.upcase
    assert_not other.valid?
  end
  
  test "email validation should accept valid addresses" do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @user.email = valid_address
      assert @user.valid?
    end
  end
  
  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example. foo@bar_baz.com foo@bar+baz.com foo@bar..com]
    invalid_addresses.each do | invalid_address | 
      @user.email = invalid_address
      assert_not @user.valid?, "#{invalid_address} should be invalid"
    end
  end
  
  test "password should be present" do
    @user.password = @user.password_confirmation = " "*6
    assert_not @user.valid?
  end
  
  test "password should have a minimum length" do
    @user.password = @user.password_confirmation = "a"*5
    assert_not @user.valid?
  end
  
  test "associated photo posts should be destroyed" do
    @user.save
    @user.photo_posts.create!(photo:"1234", description: "sample photo post")
    assert_difference 'PhotoPost.count', -1 do
      @user.destroy
    end
  end
  
  test "should follow and unfollow a user" do
    user = users(:michael)
    other_user = users(:archer)
    assert_not user.following?(other_user)
    user.follow(other_user)
    assert user.following?(other_user)
    assert other_user.followers.include?(user)
    user.unfollow(other_user)
    assert_not user.following?(other_user)
  end
  
  test "feed should have the right posts" do
    michael = users(:michael)
    archer = users(:archer)
    foo = users(:foo)
    michael.photo_posts.each do |self_post|
      assert michael.feed.include?(self_post)
    end
    foo.photo_posts.each do |follow_post|
      assert michael.feed.include?(follow_post)
    end
    archer.photo_posts.each do |unfollow_post|
      assert_not michael.feed.include?(unfollow_post)
    end
  end
  
end
