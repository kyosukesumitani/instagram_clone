require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  
  def setup
    @user = users(:michael)
    @photo_post = @user.photo_posts.first
    @comment = @photo_post.comments.build(content: 'Goood!!', user_id: users(:foo).id)
  end
  
  test "should bd valid" do
    assert @comment.valid?
  end
  
  test "order should be most recent first" do
    assert_equal comments(:most_recent), Comment.first
  end
  
  test "user_id should be presence" do
    @comment.user_id = nil
    assert_not @comment.valid?
  end
  
  test "photo_post_id should be presence" do
    @comment.photo_post_id = nil
    assert_not @comment.valid?
  end
  
  test "content should be presence" do
    @comment.content = nil
    assert_not @comment.valid?
  end
  
end
