require 'test_helper'

class PhotoPostTest < ActiveSupport::TestCase
  
  def setup
    @user = users(:michael)
    @photopost = @user.photo_posts.build(photo: '12345', description: 'sample photo')
  end

  test "should be valid" do
    assert @photopost.valid?
  end
  
  test "user id should be presence" do
    @photopost.user_id = nil
    assert_not @photopost.valid?
  end

  test "description should be at most 140 characters" do
    @photopost.description = 'a'*141
    assert_not @photopost.valid?
  end
  
  test "order should be most recent first" do
    assert_equal photo_posts(:most_recent), PhotoPost.first
  end
end
