User.create!(username: "foo",
            fullname: "foo bar",
              email: "foo@bar.com",
              password: "password",
              password_confirmation: "password")


99.times do |n|
  username = Faker::Name.name
  fullname = Faker::JapaneseMedia::SwordArtOnline.real_name
  email = "example#{n+1}@railstutorial.org"
  password = "password"
  User.create!(username: username,
               fullname: fullname,
                  email: email,
               password: password,
  password_confirmation: password)
end


users = User.order(:created_at).take(6)
50.times do
  photo = Faker::File.file_name
  description = Faker::Lorem.sentence(5)
  users.each { |user| user.photo_posts.create!(photo: photo, description: description) }
end

users = User.all
user = User.find_by(username: "foo")
following = users[2..40]
followers = users[30..50]
following.each { |i| user.follow(i) }
followers.each { |i| i.follow(user) }