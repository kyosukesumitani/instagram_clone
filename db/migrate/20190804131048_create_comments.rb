class CreateComments < ActiveRecord::Migration[5.1]
  def change
    create_table :comments do |t|
      t.references :user, foreign_key: true
      t.references :photo_post, foreign_key: true
      t.text :content

      t.timestamps
    end
    add_index :comments, [:created_at, :content]
  end
end
