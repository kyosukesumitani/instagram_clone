class CreatePhotoPosts < ActiveRecord::Migration[5.1]
  def change
    create_table :photo_posts do |t|
      t.string :photo
      t.text :description
      t.references :user, foreign_key: true

      t.timestamps
    end
    add_index :photo_posts, [:description, :user_id, :created_at]
  end
end
