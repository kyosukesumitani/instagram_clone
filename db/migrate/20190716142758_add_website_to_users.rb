class AddWebsiteToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :website, :text
    add_column :users, :introduction, :text
    add_column :users, :sex, :string
  end
end
